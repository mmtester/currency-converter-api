rem calculate code coverage
cd %1

"..\..\packages\OpenCover.4.6.519\tools\OpenCover.Console.exe" ^
-register ^
-target:"..\..\packages\NUnit.ConsoleRunner.3.9.0\tools\nunit3-console.exe" ^
-targetargs:"UnitTests\bin\Release\UnitTests.dll " ^
-filter:"+[*]* -[UnitTests]UnitTests.* -[nunit.framework]*" ^
-mergebyhash ^
-output:"codecoveragereport.xml"

"..\..\packages\ReportGenerator.4.0.4\tools\net47\ReportGenerator.exe" -reports:"codecoveragereport.xml" -targetdir:"HTML_Report"

"..\..\packages\OpenCoverToCoberturaConverter.0.3.4\tools\OpenCoverToCoberturaConverter" -input:codecoveragereport.xml -output:cobertura.xml -sources:.\