﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace UnitTests
{

    [TestFixture]
    public class CurrencyConverterUnitTests
    {
        [Test]
        public void PlaceholderTest()
        {
            Assert.AreEqual(true, true);

        }

        [Test]
        public void ConvertTest()
        {
            var converter = new CurrencyConverter.Converter();

            var basic = converter.Convert(1, CurrencyConverter.Enums.CurrencyType.USD, CurrencyConverter.Enums.CurrencyType.EUR);
            Console.WriteLine(basic);
            Assert.IsNull(basic);
        }

    }
}
